package br.com.mastertech.payzen.ws.exception;

import java.util.Collections;
import java.util.List;

public class PayzenException extends RuntimeException {

    private final List<String> reasons;

    public PayzenException(String message) {
        super(message);
        reasons = null;
    }

    public PayzenException(String message, Throwable cause) {
        super(message, cause);
        reasons = null;
    }

    public PayzenException(Throwable cause) {
        super(cause);
        reasons = null;
    }

    public PayzenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        reasons = null;
    }

    public PayzenException(String message, List<String> reasons) {
        super(message);
        this.reasons = reasons;
    }

    public PayzenException(String message, String reason) {
        super(message);
        this.reasons = Collections.singletonList(reason);
    }
}
