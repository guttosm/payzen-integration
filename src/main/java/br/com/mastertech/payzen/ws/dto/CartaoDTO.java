package br.com.mastertech.payzen.ws.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CartaoDTO {
    private String numero;
    private String nomeTitular;
    private String mesValidade;
    private String anoValidade;
    private String codigoSeguranca;
}