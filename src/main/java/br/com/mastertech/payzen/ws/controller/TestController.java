package br.com.mastertech.payzen.ws.controller;

import br.com.mastertech.payzen.core.client.PayZenGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    private PayZenGateway payZenGateway;

    @Autowired
    public TestController(PayZenGateway payZenGateway) {
        this.payZenGateway = payZenGateway;
    }

    @PostMapping("${ms-payzen.request.mapping.internal.cartao.test}")
    public Boolean test(){
        payZenGateway.createPayment();
        return Boolean.TRUE;
    }
}