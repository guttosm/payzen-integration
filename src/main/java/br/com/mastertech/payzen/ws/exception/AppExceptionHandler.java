package br.com.mastertech.payzen.ws.exception;

import br.com.mastertech.payzen.core.util.Message;
import br.com.mastertech.payzen.core.util.helper.MessageHelper;
import br.com.mastertech.payzen.ws.dto.RespostaPadrao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@ControllerAdvice
public class AppExceptionHandler {

    @Autowired
    private Message message;

    @Autowired
    private ErrorAttributes errorAttributes;

    @ResponseBody
    @ExceptionHandler(value = {Exception.class})
    public RespostaPadrao<Void> exception(HttpServletRequest request, WebRequest webRequest, Exception ex) {
        return getRespostaPadrao(request, ex, obterListaDeErros(webRequest));
    }

    @ResponseBody
    @ExceptionHandler(value = {PayzenException.class})
    public RespostaPadrao<Void> exception(HttpServletRequest request, WebRequest webRequest, PayzenException ex) {
        RespostaPadrao<Void> erroDTO = getRespostaPadrao(request, ex, obterListaDeErros(webRequest));
        return erroDTO;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
    public RespostaPadrao<Void> methodArgumentTypeMismatchException(HttpServletRequest request, WebRequest webRequest, Exception ex) {
        RespostaPadrao<Void> erroDTO = getRespostaPadrao(request, ex, obterListaDeErros(webRequest));
        erroDTO.addErro(message.getMessage("bad.request"));
        return erroDTO;
    }

    /**
     * Extrai uma lista de erros padrões do springboot
     *
     * @param request
     * @return
     */
    private List<String> obterListaDeErros(WebRequest request) {
        Set<Map.Entry<String, Object>> values = errorAttributes.getErrorAttributes(request, true).entrySet();
        List<String> erros = new ArrayList<>();
        values.forEach(o -> {
            if ("message".equalsIgnoreCase(o.getKey())) {
                erros.add(String.valueOf(o.getValue()));
            }
        });
        return erros;
    }

    /**
     * Constroi uma resposta padrão.
     *
     * @param request
     * @param ex
     * @param motivos
     * @return
     */
    private RespostaPadrao<Void> getRespostaPadrao(HttpServletRequest request, Exception ex, List<String> motivos) {
        String url = request.getRequestURL().toString();
        String causa = ex.getMessage();
        RespostaPadrao<Void> erroDTO = RespostaPadrao.<Void>builder()
                .valido(false)
                .motivos(motivos)
                .build();
        log.info(message.getMessage(MessageHelper.ERRO_APLICACAO_NEGOCIO, url, causa));
        log.error("ERRO:", ex);
        return erroDTO;
    }

}
