package br.com.mastertech.payzen.ws.controller;

import br.com.mastertech.payzen.core.service.CartaoService;
import br.com.mastertech.payzen.ws.dto.CartaoDTO;
import br.com.mastertech.payzen.ws.exception.PayzenException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class CartaoController {

    private CartaoService cartaoService;

    @Autowired
    public CartaoController(CartaoService cartaoService) {
        this.cartaoService = cartaoService;
    }

    @PostMapping("${ms-payzen.request.mapping.internal.cartao.enviar-dados}")
    public Boolean enviarDadosParaPayZen(@RequestBody CartaoDTO cartaoRequest) throws PayzenException {
        return cartaoService.enviarDadosPayZen(cartaoRequest);
    }
}