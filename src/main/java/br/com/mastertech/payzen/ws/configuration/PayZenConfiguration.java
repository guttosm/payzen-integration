package br.com.mastertech.payzen.ws.configuration;

import br.com.mastertech.payzen.core.client.PayZenClient;
import org.springframework.context.annotation.Bean;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

public class PayZenConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("br.com.mastertech.payzen.wsdl");
        return marshaller;
    }

    @Bean
    public PayZenClient countryClient(Jaxb2Marshaller marshaller) {
        PayZenClient client = new PayZenClient();
        client.setDefaultUri("http://localhost:8080/ws");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
