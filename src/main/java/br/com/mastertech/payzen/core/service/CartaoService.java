package br.com.mastertech.payzen.core.service;

import br.com.mastertech.payzen.core.util.Message;
import br.com.mastertech.payzen.core.util.helper.MessageHelper;
import br.com.mastertech.payzen.core.util.validator.CartaoValidator;
import br.com.mastertech.payzen.ws.dto.CartaoDTO;
import br.com.mastertech.payzen.ws.exception.PayzenException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class CartaoService {

    private Message message;
    private CartaoValidator cartaoValidator;

    @Autowired
    public CartaoService(Message message, CartaoValidator cartaoValidator) {
        this.message = message;
        this.cartaoValidator = cartaoValidator;
    }

    public Boolean enviarDadosPayZen(CartaoDTO cartaoDTO) {
        List<String> erros = cartaoValidator.validarEstruturaCartao(cartaoDTO);
        if (!erros.isEmpty()) {
            String mensagemErroI18n = message.getMessage(MessageHelper.DADOS_CARTAO_INCONSISTENTE);
            throw new PayzenException(message.getMessage(MessageHelper.CARTAO_VALIDAR_ERRO), mensagemErroI18n);
        }
        return null;
    }
}
