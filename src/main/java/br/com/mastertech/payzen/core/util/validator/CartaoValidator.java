package br.com.mastertech.payzen.core.util.validator;

import br.com.mastertech.payzen.core.util.Message;
import br.com.mastertech.payzen.core.util.helper.MessageHelper;
import br.com.mastertech.payzen.ws.dto.CartaoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

@Component
public class CartaoValidator {

    private final Message mensagem;

    @Autowired
    public CartaoValidator(Message mensagem) {
        this.mensagem = mensagem;
    }

    public List<String> validarEstruturaCartao(CartaoDTO cartao) {
        List<String> erros = new ArrayList<>();
        if (!ObjectUtils.isEmpty(cartao)) {
            verificarNumero(cartao, erros);
            verificarNomeTitular(cartao, erros);
            verificarMesValidade(cartao, erros);
            verificarAnoValidade(cartao, erros);
            verificarCodigoSeguranca(cartao, erros);
        }
        return erros;
    }

    private void verificarNumero(CartaoDTO cartao, List<String> erros) {
        if (ObjectUtils.isEmpty(cartao.getNumero())) {
            String porcentagemI18n = mensagem.getMessage(MessageHelper.CAMPO_NUMERO);
            erros.add(mensagem.getMessage(MessageHelper.NUMERO_OBRIGATORIO, porcentagemI18n));
        }
    }

    private void verificarNomeTitular(CartaoDTO cartao, List<String> erros) {
        if (ObjectUtils.isEmpty(cartao.getNomeTitular())) {
            String porcentagemI18n = mensagem.getMessage(MessageHelper.CAMPO_NOME_TITULAR);
            erros.add(mensagem.getMessage(MessageHelper.NOME_TITULAR_OBRIGATORIO, porcentagemI18n));
        }
    }

    private void verificarMesValidade(CartaoDTO cartao, List<String> erros) {
        if (ObjectUtils.isEmpty(cartao.getMesValidade())) {
            String porcentagemI18n = mensagem.getMessage(MessageHelper.CAMPO_MES_VALIDADE);
            erros.add(mensagem.getMessage(MessageHelper.MES_VALIDADE_OBRIGATORIO, porcentagemI18n));
        }
    }


    private void verificarAnoValidade(CartaoDTO cartao, List<String> erros) {
        if (ObjectUtils.isEmpty(cartao.getAnoValidade())) {
            String porcentagemI18n = mensagem.getMessage(MessageHelper.CAMPO_ANO_VALIDADE);
            erros.add(mensagem.getMessage(MessageHelper.ANO_VALIDADE_OBRIGATORIO, porcentagemI18n));
        }
    }

    private void verificarCodigoSeguranca(CartaoDTO cartao, List<String> erros) {
        if (ObjectUtils.isEmpty(cartao.getCodigoSeguranca())) {
            String porcentagemI18n = mensagem.getMessage(MessageHelper.CAMPO_CODIGO_SEGURANCA);
            erros.add(mensagem.getMessage(MessageHelper.CODIGO_SEGURANCA_OBRIGATORIO, porcentagemI18n));
        }
    }

}