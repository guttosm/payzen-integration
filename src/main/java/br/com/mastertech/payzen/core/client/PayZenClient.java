package br.com.mastertech.payzen.core.client;

import br.com.mastertech.payzen.wsdl.GetPaymentDetailsResponse;
import br.com.mastertech.payzen.wsdl.GetPaymentUuidResponse;
import br.com.mastertech.payzen.wsdl.GetSubscriptionDetailsResponse;
import br.com.mastertech.payzen.wsdl.GetTokenDetailsResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class PayZenClient extends WebServiceGatewaySupport {

    public GetPaymentDetailsResponse getPaymentDetails(GetPaymentDetailsResponse.GetPaymentDetailsResult paymentDetailsResult) {
        GetPaymentDetailsResponse request = new GetPaymentDetailsResponse();
        request.setGetPaymentDetailsResult(paymentDetailsResult);
        GetPaymentDetailsResponse response = (GetPaymentDetailsResponse) getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback(""));
        return response;
    }

    public GetPaymentUuidResponse getPaymentUuidDetails(GetPaymentUuidResponse.LegacyTransactionKeyResult legacyTransactionKeyResult) {
        GetPaymentUuidResponse request = new GetPaymentUuidResponse();
        request.setLegacyTransactionKeyResult(legacyTransactionKeyResult);
        GetPaymentUuidResponse response = (GetPaymentUuidResponse) getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback(""));
        return response;
    }

    public GetSubscriptionDetailsResponse getSubscriptionDetails(GetSubscriptionDetailsResponse.GetSubscriptionDetailsResult subscriptionDetailsResult) {
        GetSubscriptionDetailsResponse request = new GetSubscriptionDetailsResponse();
        request.setGetSubscriptionDetailsResult(subscriptionDetailsResult);
        GetSubscriptionDetailsResponse response = (GetSubscriptionDetailsResponse) getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback(""));
        return response;
    }

    public GetTokenDetailsResponse getTokenDetails(GetTokenDetailsResponse.GetTokenDetailsResult tokenDetailsResult) {
        GetTokenDetailsResponse request = new GetTokenDetailsResponse();
        request.setGetTokenDetailsResult(tokenDetailsResult);
        GetTokenDetailsResponse response = (GetTokenDetailsResponse) getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback(""));
        return response;
    }
}