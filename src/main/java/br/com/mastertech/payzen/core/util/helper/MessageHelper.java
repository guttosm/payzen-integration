package br.com.mastertech.payzen.core.util.helper;

public class MessageHelper {
    public static final String ERRO_APLICACAO_NEGOCIO = "erro.aplicacao.negocio";
    public static final String NUMERO_OBRIGATORIO = "numero.obrigatorio";
    public static final String NOME_TITULAR_OBRIGATORIO = "nome.titular.obrigatorio";
    public static final String MES_VALIDADE_OBRIGATORIO = "mes.validade.obrigatorio";
    public static final String ANO_VALIDADE_OBRIGATORIO = "ano.validade.obrigatorio";
    public static final String CODIGO_SEGURANCA_OBRIGATORIO = "codigo.seguranca.obrigatorio";
    public static final String DADOS_CARTAO_INCONSISTENTE = "dados.cartao.inconsistente";
    public static final String CARTAO_VALIDAR_ERRO = "cartao.validar.erro";
    public static final String CAMPO_NUMERO = "campo.numero";
    public static final String CAMPO_NOME_TITULAR = "campo.nome.titular";
    public static final String CAMPO_CODIGO_SEGURANCA = "campo.codigo.seguranca";
    public static final String CAMPO_MES_VALIDADE = "campo.mes.validade";
    public static final String CAMPO_ANO_VALIDADE = "campo.ano.validade";
}