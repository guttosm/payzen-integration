package br.com.mastertech.payzen.core.client;


import com.lyra.vads.ws.v5.ThreeDSMode;
import eu.payzen.webservices.sdk.builder.PaymentBuilder;
import eu.payzen.webservices.sdk.builder.request.CardRequestBuilder;
import eu.payzen.webservices.sdk.builder.request.OrderRequestBuilder;
import eu.payzen.webservices.sdk.builder.request.PaymentRequestBuilder;
import eu.payzen.webservices.sdk.builder.request.ThreeDSRequestBuilder;
import org.springframework.stereotype.Service;

import static eu.payzen.webservices.sdk.Payment.create;

@Service
public class PayZenGateway {

    public void createPayment(){
        OrderRequestBuilder orderRequestBuilder
                = OrderRequestBuilder
                .create()
                .orderId("TestTRS");

        PaymentRequestBuilder paymentRequestBuilder
                = PaymentRequestBuilder
                .create()
                .amount(100)
                .currency(978);

        CardRequestBuilder cardRequestBuilder
                = CardRequestBuilder
                .create()
                .number("4970100000000003")
                .scheme("CB")
                .expiryMonth(12)
                .expiryYear(2017)
                .cardSecurityCode("123");

        ThreeDSRequestBuilder threeDSRequestBuilder
                = (ThreeDSRequestBuilder) ThreeDSRequestBuilder
                .create()
                .mode(ThreeDSMode.ENABLED_CREATE);

        create(PaymentBuilder
                        .getBuilder()
                        .order(orderRequestBuilder.build())
                        .payment(paymentRequestBuilder.build())
                        .card(cardRequestBuilder.build())
                        .threeDS(threeDSRequestBuilder.build())
                        .comment("Test payment")
                        .buildCreate(),
                (result) -> System.out.println("Payment Done")
        );

    }



}
