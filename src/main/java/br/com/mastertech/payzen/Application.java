package br.com.mastertech.payzen;

import br.com.mastertech.payzen.ws.configuration.JacksonConfiguration;
import br.com.mastertech.payzen.ws.configuration.ModuleConfiguration;
import br.com.mastertech.payzen.ws.configuration.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        new SpringApplicationBuilder(
                ModuleConfiguration.class,
                Application.class,
                SwaggerConfiguration.class,
                JacksonConfiguration.class
        ).run(args);
    }


}
